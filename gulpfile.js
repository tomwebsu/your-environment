(function(){

  // Load plugins
  var gulp = require('gulp'),
      sass = require('gulp-ruby-sass'),
      autoprefixer = require('gulp-autoprefixer'),
      cssnano = require('gulp-cssnano'),
      jshint = require('gulp-jshint'),
      uglify = require('gulp-uglify'),
      imagemin = require('gulp-imagemin'),
      rename = require('gulp-rename'),
      concat = require('gulp-concat'),
      notify = require('gulp-notify'),
      cache = require('gulp-cache'),
      del = require('del'),
      sourcemaps = require('gulp-sourcemaps'),
      browserSync = require('browser-sync').create();

  var styleSources = 'src/stylesheets/**/*.scss',
      styleDestDir = 'stylesheets',
      jsSources = 'src/scripts/*.js',
      jsDestDir = 'scripts',
      imageSources = 'src/images/*',
      imageDestDir = 'images',
      devUrl = 'https://your-environment.worldsecuresystems.com/',
      browserSyncEnabled = false;


  // Styles
  gulp.task('styles', function() {
    return sass(styleSources, { style: 'expanded', sourcemap: true })
      .pipe(autoprefixer('last 2 version'))
      .pipe(sourcemaps.write())
      .pipe(gulp.dest(styleDestDir))
      .pipe(rename({ suffix: '.min' }))
      .pipe(cssnano({zindex:false}))
      .pipe(sourcemaps.write())
      .pipe(gulp.dest(styleDestDir))
      .pipe(browserSync.stream())
      .pipe(notify({ message: 'Styles task complete' }));
  });

  // Scripts
    // .pipe(jshint('.jshintrc'))
    // .pipe(jshint.reporter('default'))
  gulp.task('scripts', function() {
    return gulp.src(jsSources)
      .pipe(concat('main.js'))
      .pipe(gulp.dest(jsDestDir))
      .pipe(rename({ suffix: '.min' }))
      .pipe(uglify().on('error', function(uglify) {
        console.error(uglify);
        this.emit('end');
      }))
      .pipe(gulp.dest(jsDestDir))
      .pipe(notify({ message: 'Scripts task complete' }));
  });

  // Images
  gulp.task('images', function() {
    return gulp.src(imageSources)
      .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
      .pipe(gulp.dest(imageDestDir))
      .pipe(notify({ message: 'Images task complete' }));
  });

  // Clean
  gulp.task('clean', function() {
    return del([jsDestDir, styleDestDir, imageDestDir]);
  });

  // Default task
  gulp.task('default', ['clean'], function() {
    gulp.start('styles', 'scripts', 'images');
  });


  // Watch
  gulp.task('watch', function() {

    if (browserSyncEnabled) {
      browserSync.init({
          proxy: devUrl,
          notify: false
      });
    }
    // Watch .scss files
    gulp.watch( styleSources, ['styles']);

    // Watch .js files
    gulp.watch( jsSources, ['scripts']);

    // Watch image files
    gulp.watch( imageSources, ['images']);

  });
})()
